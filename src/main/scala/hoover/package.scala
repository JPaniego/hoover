package hoover

object `package`{
  type RoomSize = Coord
  type HooverInitialCoord = Coord
  type DirtPositions = Iterable[Coord]
  type CommandSeq = Iterable[Command]
}

