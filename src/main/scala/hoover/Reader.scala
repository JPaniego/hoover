package hoover

import java.io.File

import scala.util.{Try,Success}

trait Reader {

  type ReadResult = (Room,HooverInitialCoord,DirtPositions,CommandSeq)

  def readFile(path: String):Try[ReadResult] =
    Try[ReadResult]{
      val lines = scala.io.Source.fromFile(new File(path)).getLines()
      val (first::second::Nil) = lines.take(2).toList
      val parsed = lines.map(line => parseCoord(line).recover{
        case ParserException(l) => parseCommands(l).get
      })
      val (coords,commands) = parsed.partition{
        case Success(_:Coord) => true
        case Success(_:CommandSeq) => false
      }
      val Coord(xSize,ySize) = parseCoord(first).get
      (
        Room(xSize,ySize),
        parseCoord(second).get,
        coords.map(_.get).toIterable.asInstanceOf[DirtPositions],
        commands.map(_.get).asInstanceOf[Iterator[CommandSeq]].toIterable.flatten)
    }

  private def parseCoord(line: String): Try[Coord] = Try{
    line.split(" ").toList match {
      case ((x:String)::(y:String)::Nil) => Coord(x.toInt,y.toInt)
    }
  }.recover{
    case _: Throwable => throw new ParserException(line)
  }

  private def parseCommands(line: String): Try[CommandSeq] = Try{
    (Seq[Command]() /: line)((commands,char) => char.toUpper match {
      case 'N' => commands :+ N
      case 'S' => commands :+ S
      case 'E' => commands :+ E
      case 'W' => commands :+ W
    })
  }

  private case class ParserException(line: String) extends Exception

}
