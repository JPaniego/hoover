package hoover

trait Room {

  val values: IndexedSeq[IndexedSeq[Boolean]]

  lazy val size: Coord = Coord(values.length,values.head.length)

  def isDirty(pos: Coord): Boolean =
    values(pos.x)(pos.y)

  def isClean(pos: Coord): Boolean =
    !isDirty(pos)

  def dirty(pos: Coord, dirt: Boolean = true): Room =
    Room(values.updated(pos.x,values(pos.x).updated(pos.y,dirt)))

  def clean(pos: Coord): Room = dirty(pos,dirt=false)

  override def toString: String = (0 to values.length-1).map{
    x => ("" /: values(x))((s,bool) => s"""$s${if (bool) "x " else "o "}""")
  }.mkString("\n")

}

object Room {
  def apply(xSize: Int,ySize: Int): Room = new Room {
    val values = (1 to xSize).map(_=>
      (1 to ySize).map(_=>false))
  }
  def apply(positions: IndexedSeq[IndexedSeq[Boolean]]): Room = new Room {
    val values = positions
  }

}