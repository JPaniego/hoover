package hoover

sealed trait Command{
  def apply(coord: Coord,room:RoomSize):Coord
}
case object N extends Command{
  def apply(coord:Coord,room: RoomSize):Coord = {
    (coord, room) match {
      case (Coord(x, y), Coord(_, maxY)) =>
        val newY = y+1
        Coord(x,if ( newY >= maxY) maxY else newY)
    }
  }
}
case object S extends Command{
  def apply(coord:Coord,room: RoomSize):Coord = {
    (coord, room) match {
      case (Coord(x, y), Coord(_, maxY)) =>
        val newY = y-1
        Coord(x,if ( newY <= 0) 0 else newY)
    }
  }
}
case object E extends Command{
  def apply(coord:Coord,room: RoomSize):Coord = {
    (coord, room) match {
      case (Coord(x, y), Coord(maxX, _)) =>
        val newX = x+1
        Coord(if ( newX >= maxX) maxX else newX,y)
    }
  }
}
case object W extends Command{
  def apply(coord:Coord,room: RoomSize):Coord = {
    (coord, room) match {
      case (Coord(x, y), Coord(maxX, _)) =>
        val newX = x-1
        Coord(if ( newX <=0) 0 else newX,y)
    }
  }
}