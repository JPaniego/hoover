package hoover

import scala.util.Success

object Boot extends App with Reader{

  //  Read file

  val Success((scenario,initialPos,dirt,commands)) =
    readFile("src/main/resources/input.txt")

  //  Dirty specified positions

  val dirtyRoom =
    (scenario /: dirt)((room,position) => room.dirty(position))

  println(s"Dirty room:\n$dirtyRoom\n")

  //  Execute hoover routine

  val (roomState,cleanedPatches,finalPosition) = ((dirtyRoom,0,initialPos) /: commands){
    case ((room,patches,pos),command) =>
      val newCoord = command(pos,room.size)
      val isNewPatch = if (room.isDirty(newCoord)) 1 else 0
      (room.clean(newCoord),patches + isNewPatch, newCoord)
  }

  //  Show up results

  println(s"Clean? room:\n$roomState\n\n${finalPosition.x} ${finalPosition.y}\n$cleanedPatches")

}
